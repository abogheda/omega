
class StackModel {
  String doctorName;
  String image;
  int id ;
  String rate;
  String location;
  String distance;


  StackModel({
    required this.doctorName,
    required this.image,
    required this.rate,
    required this.distance,
    required this.location,
    required this.id,
  });
}