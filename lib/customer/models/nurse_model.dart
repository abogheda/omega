import 'package:flutter/material.dart';

class NurseModel {
  String image;
  int id ;
  String title;
  String rate;
  String distance;
  Color color;

  NurseModel({
    required this.image,
    required this.title,
    required this.rate,
    required this.distance,
    required this.color,
    required this.id,
  });
}