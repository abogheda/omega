class NurseDetailsModel {
  String image;
  int id ;
  String title;
  String number;
  List<String> times;

  NurseDetailsModel({
    required this.image,
    required this.title,
    required this.number,
    required this.times,
    required this.id,
  });
}