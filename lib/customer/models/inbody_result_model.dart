
class InbodyResultModel {
  int id;
  String breakFast;
  String? lunch;
  String lastMeal;
  String? beforeWorkout;
  String? duringWorkout;
  String? afterWorkout;
  String? notes;

  InbodyResultModel({
    required this.breakFast,
     this.lunch,
    required this.lastMeal,
     this.notes,
    this.beforeWorkout,
    this.duringWorkout,
    this.afterWorkout,
    required this.id,
  });
}