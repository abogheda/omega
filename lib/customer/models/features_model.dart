import 'package:flutter/material.dart';

class FeaturesModel {
  String image;
  int id ;
  String title;
  Color color;

  FeaturesModel({
    required this.image,
    required this.title,
    required this.color,
    required this.id,
  });
}