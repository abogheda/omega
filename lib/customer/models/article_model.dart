import 'package:flutter/material.dart';

class ArticleModel {
  String site;
  int id ;
  String title;
  Color color;

  ArticleModel({
    required this.site,
    required this.title,
    required this.color,
    required this.id,
  });
}