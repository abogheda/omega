
class AppointmentModel {
  String testName;
  String date;
  String price;
  String image;
  String? time;
  int id ;



  AppointmentModel({
    required this.testName,
    required this.price,
    required this.image,
    required this.date,
    required this.id,
    this.time
  });
}