import 'package:base_flutter/res.dart';

import '../models/calender_model.dart';

List<CalenderModel> testsDummyList = [
  CalenderModel(
      date:"Tomorrow",
      testName: 'PCR Test',
      id: 1, image: Res.test),
  CalenderModel(
      date:"1/3/2023",
      testName: 'CPC Test',
      id: 1, image: Res.test),
  CalenderModel(
      date: "10/3/2023",testName: 'Blood Test',
      id: 2, image: Res.test),
];
