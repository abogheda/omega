import 'package:base_flutter/customer/models/appointment_model.dart';
import 'package:base_flutter/res.dart';
List<AppointmentModel> previousAppointmentDummyList = [
  AppointmentModel(
      date:"1/2/2023",
      testName: 'Dr Ahmed Goda',
      id: 1, image: Res.doc1, price: '200'),
  AppointmentModel(
      date:"11/2/2023",
      testName: 'Dr Samir Sameh',
      id: 2, image: Res.doctorTwo, price: '500'),
  AppointmentModel(
      date: "09/1/2023",testName: 'Dr Ahmed Yasser',
      id: 3, image: Res.doc3, price: '340'),
];

List<AppointmentModel> comingAppointmentDummyList = [
  AppointmentModel(
    time: "8:00 pm",
      date:"1/3/2023",
      testName: 'Dr Nader Elsayed',
      id: 1, image: Res.doctorFour, price: '200'),
  AppointmentModel(
      time: "4:00 pm",
      date:"11/4/2023",
      testName: 'Dr Samir Sameh',
      id: 2, image: Res.doctorTwo, price: '500'),
];
