import 'package:base_flutter/res.dart';

import '../models/calender_model.dart';

List<CalenderModel> drugsDummyList = [
  CalenderModel(
      date:"Today at 3:00pm",
      testName: 'Pressure Medication',
      id: 1, image: Res.medicine),
  CalenderModel(
      date:"Today at 3:00pm",
      testName: 'Sugar Medicine',
      id: 1, image: Res.medicine),
  CalenderModel(
      date: "Today at 4:00pm",testName: 'Phenadone',
      id: 2, image: Res.medicine),
];
