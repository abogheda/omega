import 'package:base_flutter/customer/models/nurse_details.dart';
import 'package:base_flutter/res.dart';


List<NurseDetailsModel> nurseDetailsDummyList = [
  NurseDetailsModel(id: 1, image: Res.nurse1, number: '0100235482', times: ["3:00 Pm","4:00 Pm","5:00 Pm","6:00 Pm","7:00 Pm"], title: 'Nada Ahmed'),
  NurseDetailsModel(id: 2, image: Res.nurse2, number: '0100455742', times: ["12:00 am","1:00 am","2:00 am","3:00 am","4:00 am","5:00 am","6:00 am","7:00 am"], title: 'Ahmed Mohamed'),
  NurseDetailsModel(id: 3, image: Res.nurse3, number: '0110235333', times: ["1:00 Pm","2:00 Pm","3:00 Pm","4:00 Pm",], title: 'Nehal Mostafa'),

];
