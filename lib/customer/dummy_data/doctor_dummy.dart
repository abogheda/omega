import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

import '../models/features_model.dart';

List<FeaturesModel> doctorsList = [
  FeaturesModel(
      image:Res.doctorOne,
      title: 'Zain Malek',
      color: Color(0xffDAF8E3),
      id: 1),
  FeaturesModel(
      image: Res.doctorTwo,title: 'Ahmed Gamal',
      color: Color(0xffC2E2EB),
      id: 2),
  FeaturesModel(
      image:Res.doctorThree,
      title: 'Toqa Elbauomy',
      color: Color(0xffF7FFCD),
      id: 3),
  FeaturesModel(
      image: Res.doctorFour,title: 'Khaled Elbauomy',
      color: Color(0xffB0EAE6),
      id: 4),
];
