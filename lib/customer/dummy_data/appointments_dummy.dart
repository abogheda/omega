import 'package:base_flutter/res.dart';

import '../models/calender_model.dart';

List<CalenderModel> appointmentsDummyList = [
  CalenderModel(
      date:"Tomorrow",
      testName: 'Dentist\'s Appointment',
      id: 1, image: Res.doctorOne),
  CalenderModel(
      date: "1/3/2023",testName: 'Gastroenterology\'s Appointment',
      id: 2, image: Res.doctorTwo),
];
