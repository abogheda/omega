import 'package:base_flutter/customer/models/inbody_result_model.dart';

List<InbodyResultModel> inBodyResultDummyList = [
  InbodyResultModel(
    breakFast: '''50 gm cream of rice 
1 scoop whey protein
100 gm of fruit of choice 
30 gm nut of choice ''',
    beforeWorkout: '''200 gm sweat potato
1 table spoon peanut butter''',
    duringWorkout: '''1 scoop whey protein''',
    afterWorkout: '''200 gm white rice
150 chicken breasts''',
    lastMeal: '''150 gm mash potatoes
2 full eggs
2 egg whites
- snacks throughout the day : one banana, any amount of watermelon / strawberry
- Exchange sugar with artificial ones''',
    id: 1,
    notes: '''- no fruits before bed 
- at least 2000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''80 gm oats 
3 full eggs
15 gm olive oil
2 dark toasts''',
    lastMeal: '''1 can of tuna 
100 gm madh potato
15 gm olive oil''',
    beforeWorkout: '''200 gm sweat potato
1 table spoon peanut butter''',
    duringWorkout: '''1 scoop whey protein''',
    afterWorkout: '''200 gm white rice
150 chicken breasts''',
    id: 2,
    notes: '''- no fruits before bed 
- at least 2000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
      breakFast:
          '''- First thing in the morning before eating: drink 500 ml water mixed with apple cider
Then have breakfast: 
2 whole eggs and 2 egg whites 
2 dark toasts
150 gm cottage chease with 15 gm olive oil added to .''',
      beforeWorkout: '''Around one hour before working out 
150 gm basmati rice
150 gm salmon
15 gm olive oil
''',
      duringWorkout:
          '''1 piece rice cake with one table spoon suger free jam spread over it 
One scoop whey protein
One scoop creatine ''',
      afterWorkout: '''200 gm basmati rice 
200 gm lean beaf
A plate of salade ''',
      lastMeal:
          ''' Can of tuna ( or 100 gm chicken breasts) whichever is more convenient
With 100 gm brown rice ''',
      notes: '''- no fruits before bed 
- at least 2000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
      id: 3),
  InbodyResultModel(
    breakFast: '''Morning smoothie: 
75 gm of oats
1 scoop whey protein
1 tablespoon peanut butter
50 gm blue perry
In the blender then mix ''',
    beforeWorkout: '''-An hour to an hour and half before workout :
150 gm white rice
150 gm chicken breasts 
A plate of salade 10 gm  olive oil''',
    duringWorkout: '''Half an apple 
Then the meal : 
150 gm white rice
150 gm lean beef ( or  chicken breasts)''',
    lastMeal: '''Night smoothie: 
70 gm oats 
1 scoop whey protein
30 gm nut of choice 
In the blinder then mix''',
    id: 4,
    notes: '''- no fruits before bed 
- at least 2000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),

  InbodyResultModel(
    breakFast: '''First thing in the morning before eating: drink 700 ml water mixed with apple cider
Then have breakfast:
3whole eggs and3 egg whites
3 dark toasts
200 gm cottage cheese with 15 gm olive oil added to . ''',
    beforeWorkout: '''Around one hour before working out
200 gm basmati rice
250 gm salmon
15 gm olive oil
30 minutes before working out:
2 bananas and cup of coffee''',
    duringWorkout: '''2 pieces rice cake with one table spoon sugar free jam spread over it
2 scoop whey protein
One scoop creatine''',
    afterWorkout: '''250 gm basmati rice
300 gm lean beef
A plate of salade ''',
    lastMeal: '''150gm chicken breasts
With 100 gm brown rice ''',
    id: 5,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''150 gm oats
4 full eggs
15 gm olive oil
3 dark toasts ''',
    beforeWorkout: '''250gm sweat potato
2 table spoon peanut butter''',
    duringWorkout: '''2 scoop whey protein''',
    afterWorkout: '''250 gm white rice
300 chicken breasts''',
    lastMeal: '''150 gm chicken breasts
100 gm mash potato
15 gm olive oil ''',
    id: 6,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: ''' First thing in the morning before eating: drink 400 ml water mixed with apple cider
Then have breakfast:
1 whole eggs and 1 egg whites
1 dark toasts
100 gm cottage cheese with 110 gm olive oil added to . ''',
    beforeWorkout: '''Around one hour before working out
100 gm basmati rice
70 gm salmon
15 gm olive oil
30 minutes before working out:
A banana and cup of coffee''',
    duringWorkout: '''1 piece rice cake with one table spoon sugar free jam spread over it
One scoop whey protein
One scoop creatine ''',
    afterWorkout: '''150 gm basmati rice
150 gm lean beef
A plate of salade''',
    lastMeal: '''Can of tuna ( or 100 gm chicken breasts) whichever is more convenient
With 100 gm brown rice  ''',
    id: 7,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''Morning smoothie:
80 gm of oats
1 scoop whey protein
2 tablespoon peanut butter
70 gm blue perry
In the blender then mix''',
    beforeWorkout: '''-An hour to an hour and half before workout :
170 gm white rice
200 gm chicken breasts
A plate of salade 15 gm olive oil
-Half hour before workout :
150 gm Sweat potato
One scoop Pre-workout''',
    duringWorkout: '''Half an apple
Then the meal :
170gm white rice
200 gm mullet fish ''',
    afterWorkout: '''Rest only''',
    lastMeal: '''Night smoothie:
100 gm oats
1 scoop whey protein
40 gm nut of choice
In the blinder then mix''',
    id: 8,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''Morning smoothie:
75 gm of oats
1 scoop whey protein
1 tablespoon peanut butter
50 gm blue perry
In the blender then mix ''',
    beforeWorkout: '''-An hour to an hour and half before workout :
150 gm white rice
150 gm chicken breasts
A plate of salade 10 gm olive oil
-Half hour before workout :
100 gm Sweat potato
One scoop Pre-workout''',
    duringWorkout: '''Half an apple
Then the meal :
150 gm white rice
150 gm lean beef ( or chicken breasts)''',
    afterWorkout: '''Rest only''',
    lastMeal: '''Night smoothie:
70 gm oats
1 scoop whey protein
30 gm nut of choice
In the blinder then mix''',
    id: 9,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''50 gm nut of choice
1 dark toasts
60 gm of oats
2 full eggs ''',
    beforeWorkout: '''100 gm mash potato
100 gm chicken breasts
90 gm white rice Half hour before workout
A banana and cup of coffee
2 table spoon peanut butter
''',
    duringWorkout: '''1 scoop when protein
1 scoop creatine 
''',
    afterWorkout: '''Rest only''',
    lastMeal: '''can of tuna 
100 gm brown rice
50 gm oats
20 gm nut of choice''',
    id: 10,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''1 scoop protein
30 gm blue perry
60 gm of oats''',
    beforeWorkout: '''half an apple
20 gm olive oil
100 gm salmon
Half hour before workout
100 gm basmatic rice
1 scoop protein''',
    duringWorkout: '''1 piece rice cake with one table spoon suger free''',
    afterWorkout: '''Rest only''',
    lastMeal: '''50 gm oats 
1 scoop protein
1 can of tuna''',
    id: 11,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''150 gm oats
4 full eggs
15 gm olive oil
3 dark toasts ''',
    beforeWorkout: '''200 gm sweat potato
2 table spoon peanut butter
50 gm madh potato''',
    duringWorkout: '''2 scoop whey protein''',
    afterWorkout: '''250 gm white rice
300 chicken breasts''',
    lastMeal: '''1 can of tuna
50 gm madh potato
1 scoop protein ''',
    id: 12,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''2 full eggs
1 dark toasts
1 scoop protein
''',
    beforeWorkout: '''90 gm white rice
150 gm chicken breasts
110 gm salmon''',
    duringWorkout: '''2 scoop whey protein''',
    afterWorkout: '''250 gm white rice
300 chicken breasts''',
    lastMeal: '''1 can of tuna
50 gm madh potato
1 scoop protein ''',
    id: 13,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''3 full eggs
2 dark toasts
2 scoop protein
''',
    beforeWorkout: '''90 gm white rice
150 gm chicken breasts
110 gm salmon''',
    duringWorkout: '''1 scoop protein
90 gm blue perry''',
    afterWorkout: '''250 gm white rice
300 chicken breasts''',
    lastMeal: '''90 gm brown rice
50 gm oats
1 scoop protein''',
    id: 14,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),
  InbodyResultModel(
    breakFast: '''2 whole eggs and 2 egg whites
150 gm cottage cgease with 15 gm olive oil added to
1 dark toasts
''',
    beforeWorkout: '''half an apple
30 gm olive oil
1 scoop protein''',
    duringWorkout: '''1 scoop protein
90 gm blue perry''',
    afterWorkout: '''250 gm white rice
300 chicken breasts''',
    lastMeal: '''90 gm brown rice
50 gm oats
1 scoop protein''',
    id: 14,
    notes: '''- no fruits before bed 
- at least 3000 ml water throughout the day
- no white  flour products
- artificial sweeteners used instead of regular one
''',
  ),

];
