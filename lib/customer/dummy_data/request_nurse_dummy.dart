import 'package:base_flutter/customer/models/nurse_model.dart';

import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

List<NurseModel> requestNursesList = [
  NurseModel(
      image: Res.nurse1, title: 'Nada Ahmed', color: Color(0xffDAF8E3), id: 1, rate: '4', distance: '3k'),
  NurseModel(
      image: Res.nurse2,
      title: 'Ahmed Mohamed',
      color: Color(0xffC2E2EB),rate: '4', distance: '3k',
      id: 2),
  NurseModel(
      image: Res.nurse3,
      title: 'Nehal Mostafa',
      color: Color(0xffF7FFCD),rate: '3', distance: '3k',
      id: 3),

];
