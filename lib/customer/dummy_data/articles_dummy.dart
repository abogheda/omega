import 'package:base_flutter/customer/models/article_model.dart';
import 'package:flutter/material.dart';

List<ArticleModel> articlesList = [
  ArticleModel(
      site:
          """The body starts to respond to healthy dietary changes as soon as they are made. This can be advantageous, because a diet can then eventually reduce the risk of obesity, diabetes and heart disease, as well as improve a person’s overall sense of well-being.""",
      title: 'Diet and nutrition ',
      color: Color(0xffDAF8E3),
      id: 1),
  ArticleModel(
      site:
          """Eating carbohydrates increases the blood sugar level, but the extent of this rise depends on a food’s glycemic index. The glycemic index is a ranking system, based on a score of 1 to 100, that determines the effect of a food on blood sugar levels. """,
      title: 'Control of blood glucose level',
      color: Color(0xffC2E2EB),
      id: 2),
  ArticleModel(
      site:
          """Healthy food delivers nutrients to help keep your organs, muscles, and bones working properly. Junk food, meanwhile, can do the opposite.""",
      title: 'Healthy food',
      color: Color(0xffF7FFCD),
      id: 3),
  ArticleModel(
      site:
          """Diabetes is a condition that impairs the body’s ability to process blood glucose, otherwise known as blood sugar. There are several types of diabetes, which have various treatments.
Different kinds of diabetes can occur, and how people manage the condition depends on the type. Not all forms of diabetes stem from a person being overweight or leading an inactive lifestyle. Some are present from childhood.""",
      title: 'Diabetes',
      color: Color(0xffB0EAE6),
      id: 4),
];
