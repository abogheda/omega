
import 'package:base_flutter/customer/models/features_model.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

List<FeaturesModel> featuresList = [
  FeaturesModel(image: Res.human_body, title: 'Inbody Check', color: Color(0xffDAF8E3), id: 1),
  FeaturesModel(image: Res.article, title: 'Articles', color: Color(0xffC2E2EB), id: 2),
  FeaturesModel(image: Res.calendar, title: 'Calender', color: Color(0xffF7FFCD), id: 3),
  FeaturesModel(image: Res.medical_health, title: 'Stack', color: Color(0xffB0EAE6), id: 4),
  FeaturesModel(image: Res.medicalRecord, title: 'Medical Record', color: Color(0xffA7D1CE), id: 5),
  FeaturesModel(image: Res.article, title: 'Request a nurse', color: Color(0xffB7DBAF), id: 6),
];