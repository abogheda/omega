import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import '../models/features_model.dart';

List<FeaturesModel> nursesList = [
  FeaturesModel(
      image: Res.nurse1, title: 'Nada Ahmed', color: Color(0xffDAF8E3), id: 1),
  FeaturesModel(
      image: Res.nurse2,
      title: 'Ahmed Mohamed',
      color: Color(0xffC2E2EB),
      id: 2),
  FeaturesModel(
      image: Res.nurse3,
      title: 'Nehal Mostafa',
      color: Color(0xffF7FFCD),
      id: 3),
];
