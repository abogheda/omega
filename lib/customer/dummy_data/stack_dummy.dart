import 'package:base_flutter/customer/models/stack_model.dart';

import 'package:base_flutter/res.dart';


List<StackModel> stackDoctorDummyList = [
  StackModel(

      id: 1, image: Res.doc1, doctorName: 'Dr. Fred Mask', location: 'Mansoura', rate: '4.4', distance: '3.5'),
  StackModel(

      id: 2, image: Res.doc2, doctorName: 'Ahmed Bahgat', location: 'Mansoura', rate: '4.9', distance: '2.3'),
  StackModel(
      id: 3, image: Res.doc3, distance: '3.3', location: 'Mansoura', doctorName: 'Ahmed Salah', rate: '5'),
];
List<StackModel> stackPharmacyDummyList = [
  StackModel(

      id: 1, image: Res.lab1, doctorName: 'Dr. Fred Pharmacy', location: 'Mansoura', rate: '3.9', distance: '2.1'),
  StackModel(

      id: 2, image: Res.lab2, doctorName: 'Dr. Ali Pharmacy', location: 'Mansoura', rate: '4', distance: '1.3'),

];List<StackModel> stackLabsDummyList = [
  StackModel(

      id: 1, image: Res.lab5, doctorName: 'Al-nada Lab', location: 'Mansoura', rate: '4.4', distance: '3.5'),
  StackModel(

      id: 2, image: Res.lab4, doctorName: 'Al-Ahram Lab', location: 'Mansoura', rate: '4.9', distance: '2.3'),
  StackModel(
      id: 3, image: Res.lab3, distance: '3.3', location: 'Mansoura', doctorName: 'Al-Eslam Lab', rate: '5'),
];
