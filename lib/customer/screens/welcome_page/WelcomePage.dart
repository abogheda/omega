part of 'WelcomePageImports.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  final WelcomePageData welcomePageData = new WelcomePageData();

  @override
  void initState() {
    welcomePageData.initPagesData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: DefaultAppBar(title: "skip",),
      backgroundColor: MyColors.white,
      body: BlocBuilder<GenericBloc<int>, GenericState<int>>(
        bloc: welcomePageData.pagesCubit,
        builder: (_, state) {
          return AnimatedSwitcher(
            duration: Duration(milliseconds: 500),
            reverseDuration: Duration(milliseconds: 500),
            child: welcomePageData.data[state.data],
            transitionBuilder: (child, animation) {
              return FadeTransition(
                child: child,
                opacity: animation,
              );
            },
            switchInCurve: Curves.easeIn,
            switchOutCurve: Curves.easeOut,
          );
        },
      ),
    );
  }
}
