import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import '../../../general/constants/MyColors.dart';
import '../../models/WelcomeModel.dart';
import 'widgets/WelcomeWidgetImports.dart';


part 'WelcomePage.dart';
part 'WelcomePageData.dart';