part of 'WelcomePageImports.dart';
class WelcomePageData {
  GenericBloc<int> pagesCubit = new GenericBloc(0);
  List<Widget> data = [];

  initPagesData() {
    data = [
      BuildPageView(
        key: Key("1"),
        model: WelcomeModel(
          desc: "Welcome to Omega application",
          image: Res.Hello_rafiki,
          index: 0,
          first: true,
          pageCubit: pagesCubit, title: '',
        ),
      ),
      BuildPageView(
        key: Key("2"),
        model: WelcomeModel(
          desc: "Omega seek to save people's time by providing them with an experience of dealing with easy interfaces.",
          image: Res.OnlineDoctorRafiki,
          index: 1,
          pageCubit: pagesCubit, title: '',
        ),
      ),
      BuildPageView(
        key: Key("3"),
        model: WelcomeModel(
          desc: "Mobile app to alert, advise, recommend and more to take care of your health",
          image: Res.WorldHealthDayPana,
          index: 2,
          pageCubit: pagesCubit, title: '',
        ),
      ),
      BuildPageView(
        key: Key("4"),
        model: WelcomeModel(
          desc: "Let's go!",
          image: Res.Welcome_bro,
          index: 3,
          last: true,
          pageCubit: pagesCubit, title: '',
        ),
      ),
    ];
  }
}
