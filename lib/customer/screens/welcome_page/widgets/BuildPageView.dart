part of 'WelcomeWidgetImports.dart';

class BuildPageView extends StatelessWidget {
  final WelcomeModel model;

  const BuildPageView({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          AppBar(
            toolbarHeight: 100,
            automaticallyImplyLeading: false,
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            leadingWidth: 100,
            leading: Container(
              margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
              child: GestureDetector(
                onTap: () => AutoRouter.of(context).push(LoginRoute()),
                child: MyText(
                  title: tr(context, "skip"),
                  color: MyColors.black,
                  size: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Image.asset(model.image,scale: 1.4,),

          Column(
            children: [
              MyText(
                title: model.desc,
                size: 15,
                color: MyColors.grey,
                alien: TextAlign.center,
              ),
              IndicatorDots(
                dots: 4,
                model: model,
              ),

            ],
          ),
        ],
      ),
    );
  }


}
