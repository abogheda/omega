part of 'request_nurse_imports.dart';




class RequestNurse extends StatefulWidget {
  const RequestNurse({Key? key}) : super(key: key);

  @override
  State<RequestNurse> createState() => _RequestNurseState();
}

class _RequestNurseState extends State<RequestNurse> {
  List<NurseModel> nurseList = requestNursesList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(
        title: 'Request Nurse',
        isCenter: true,
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: MyText(title: "Nearby Nurses", color: MyColors.black, size: 14,fontWeight: FontWeight.bold,),
          ),
          BuildDepartmentItem(model: nurseList,),
          // SizedBox(height: 150,),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: MyText(title: "Top Rated Nurses", color: MyColors.black, size: 14,fontWeight: FontWeight.bold,),
          ),
          BuildDepartmentItem(model: nurseList,),
          SizedBox(height: 10,),
        ],
      ),
    );
  }
}
