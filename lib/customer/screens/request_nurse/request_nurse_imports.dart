import 'package:base_flutter/customer/models/nurse_model.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../general/constants/MyColors.dart';
import '../../../general/widgets/DefaultAppBar.dart';
import '../../dummy_data/request_nurse_dummy.dart';
import '../../widgets/build_department_item.dart';



part 'request_nurse.dart';
part 'request_nurse_data.dart';