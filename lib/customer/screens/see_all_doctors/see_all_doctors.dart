part of 'see_all_doctors_imports.dart';



class SeeAllDoctors extends StatefulWidget {
  final String title;
  final bool showFilter;
  final List<FeaturesModel>? model;

  const SeeAllDoctors({Key? key,required this.title,required this.showFilter,required this.model}) : super(key: key);
  @override
  State<SeeAllDoctors> createState() => _SeeAllDoctorsState();
}

class _SeeAllDoctorsState extends State<SeeAllDoctors> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(
        title: widget.title,
        isCenter: true,
      ),
      body: Column(
        children: [
          BuildSearch(),
          Expanded(
            child: GridView.builder(
              itemCount: widget.model!.length,
              itemBuilder: (_, index) {
                return SeeAllDoctorsContainer(model: widget.model![index],);
              },
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 5.0,
                mainAxisSpacing: 5.0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
