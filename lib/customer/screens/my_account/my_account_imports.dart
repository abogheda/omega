import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/my_account/widgets/my_account_widgets_imports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/http/dio/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

part 'my_account.dart';

part 'my_account_data.dart';