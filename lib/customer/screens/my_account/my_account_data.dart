part of 'my_account_imports.dart';


class MyAccountData{
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final TextEditingController name = new TextEditingController();
  final TextEditingController email = new TextEditingController();
  final TextEditingController phone = new TextEditingController();
  updateProfile(BuildContext context) async {
    // FirebaseMessaging messaging = FirebaseMessaging.instance;

    if (formKey.currentState!.validate()) {
      if (phone.text.length < 9) {
        CustomToast.showSimpleToast(msg: tr(context, "checkPhoneNumber"));
        return;
      }
      if (!RegExp(r'\S+@\S+\.\S+').hasMatch(email.text)) {
        CustomToast.showSimpleToast(msg: tr(context, "checkEmail"));
        return;
      }
      CustomToast.showSimpleToast(msg: "done");
      AutoRouter.of(context).pop();

    }
  }
}