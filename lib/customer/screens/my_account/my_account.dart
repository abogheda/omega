part of 'my_account_imports.dart';

class MyAccount extends StatefulWidget {
  const MyAccount({Key? key}) : super(key: key);

  @override
  State<MyAccount> createState() => _MyAccountState();
}

class _MyAccountState extends State<MyAccount> {

  final MyAccountData myAccountData = MyAccountData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.primary,
      body: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          Stack(
            children: [
              Container(
                // margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                height: MediaQuery.of(context).size.height * .15,
                width: double.infinity,
                decoration: BoxDecoration(color: MyColors.primary),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    icon: Icon(Icons.adaptive.arrow_back,color: MyColors.white,),
                    onPressed: ()=> AutoRouter.of(context).pop(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 6),
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: double.infinity,
                  decoration: BoxDecoration(color: MyColors.white,borderRadius: BorderRadius.circular(25),),
                  child: ListView(
                    padding: const EdgeInsets.only(top: 40,left: 15,right: 15),
                    children: [
                      BuildInputsField(myAccountData: myAccountData,),
                      SizedBox(
                        height: 50,
                      ),
                      DefaultButton(title: tr(context,"send"), onTap: (){
                        myAccountData.updateProfile(context);
                      },),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: MediaQuery.of(context).size.height*.07,
                // right: MediaQuery.of(context).size.width / 1.5,
                left: MediaQuery.of(context).size.width / 2.71,
                child: CachedImage(
                  haveRadius: true,
                  borderRadius: BorderRadius.circular(100),
                  height: 100,
                  width: 100,
                  url:
                  "https://hips.hearstapps.com/hmg-prod/images/portrait-of-a-happy-young-doctor-in-his-clinic-royalty-free-image-1661432441.jpg?crop=0.66698xw:1xh;center,top&resize=1200:*",
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
