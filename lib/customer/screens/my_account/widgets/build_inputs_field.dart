part of 'my_account_widgets_imports.dart';


class BuildInputsField extends StatelessWidget {
  final MyAccountData myAccountData;

  const BuildInputsField({Key? key,required this.myAccountData}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Form(
      key: myAccountData.formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GenericTextField(
              fieldTypes: FieldTypes.normal,
              label: tr(context, "name"),
              controller: myAccountData.name,
              margin: const EdgeInsets.symmetric(vertical: 5),
              action: TextInputAction.next,
              type: TextInputType.text,
              validate: (value) => value!.validateEmpty(context),
            ),
            GenericTextField(
              fieldTypes: FieldTypes.normal,
              label: tr(context, "phone"),
              controller: myAccountData.phone,
              margin: const EdgeInsets.symmetric(vertical: 5),
              action: TextInputAction.next,
              type: TextInputType.phone,
              validate: (value) => value!.validatePhone(context),
            ),
            GenericTextField(
              fieldTypes: FieldTypes.normal,
              label: tr(context, "mail"),
              controller: myAccountData.email,
              margin: const EdgeInsets.symmetric(vertical: 5),
              action: TextInputAction.next,
              type: TextInputType.emailAddress,
              validate: (value) => value!.validateEmail(context),
            ),
          ],
        ),);
  }
}
