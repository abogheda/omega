import 'package:base_flutter/customer/dummy_data/inbody_result_dummy.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../general/constants/MyColors.dart';
import '../../../general/widgets/DefaultAppBar.dart';
import '../../models/inbody_result_model.dart';



part 'inbody_result.dart';

part 'inbody_result_data.dart';