part of 'inbody_result_imports.dart';


class InbodyResult extends StatelessWidget {
  final int id;
  const InbodyResult({Key? key,required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int newIndex = 0;
    final List<InbodyResultModel> inBodyResultList = inBodyResultDummyList;
    if(id == inBodyResultList.length){
       newIndex = id+1;
    }else{
      newIndex = id;
    }

    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(
        title: 'Inbody Result',
        isCenter: true,
      ),
      body: ListView(
        padding: const EdgeInsets.all(10),
        children: [
          MyText(title: "BreakFast", color: MyColors.black, size: 12,fontWeight: FontWeight.bold,),
          MyText(title: inBodyResultList[newIndex].breakFast, color: MyColors.blackOpacity, size: 11,),
          Divider(
            height: 10,
            thickness: 1,
            indent: 20,
            endIndent: 20,
          ),
          MyText(title: "Before Workout", color: MyColors.black, size: 12,fontWeight: FontWeight.bold,),
          MyText(title: inBodyResultList[newIndex].beforeWorkout ?? "", color: MyColors.blackOpacity, size: 11,),
          Divider(
            height: 10,
            thickness: 1,
            indent: 20,
            endIndent: 20,
          ),
          MyText(title: "During Workout", color: MyColors.black, size: 12,fontWeight: FontWeight.bold,),
          MyText(title: inBodyResultList[newIndex].duringWorkout ?? "",color: MyColors.blackOpacity, size: 11,),
          Divider(
            height: 10,
            thickness: 1,
            indent: 20,
            endIndent: 20,
          ),
          MyText(title: "After Workout", color: MyColors.black, size: 12,fontWeight: FontWeight.bold,),
          MyText(title: inBodyResultList[newIndex].afterWorkout ?? "", color: MyColors.blackOpacity, size: 11,),
          Divider(
            height: 10,
            thickness: 1,
            indent: 20,
            endIndent: 20,
          ),
          MyText(title: "Last Meal", color: MyColors.black, size: 12,fontWeight: FontWeight.bold,),
          MyText(title: inBodyResultList[newIndex].lastMeal, color: MyColors.blackOpacity, size: 11,),
          Divider(
            height: 10,
            thickness: 1,
            indent: 20,
            endIndent: 20,
          ),
          MyText(title: "Side Notes", color: MyColors.black, size: 12,fontWeight: FontWeight.bold,),
          MyText(title: inBodyResultList[newIndex].notes ??'', color: MyColors.blackOpacity, size: 11,),
        ],
      ),
      // bottomNavigationBar: DefaultButton(onTap: () {inbodyData.calInbody();  }, title: 'Continue',),
    );
  }
}
