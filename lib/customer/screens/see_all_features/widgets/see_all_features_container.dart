part of 'see_all_features_widgets_imports.dart';

class SeeAllFeaturesContainer extends StatelessWidget {
  final FeaturesModel model;

  const SeeAllFeaturesContainer({Key? key, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        switch (model.id) {
          case 1:
            AutoRouter.of(context).push(InbodyRoute());
            break;
          case 2:
            AutoRouter.of(context).push(ArticlesRoute());
            break;
          case 3:
            AutoRouter.of(context).push(CalenderRoute());
            break;
          case 4:
            AutoRouter.of(context).push(StackScreenRoute());
            break;
          case 5:
            AutoRouter.of(context).push(MedicalRecordRoute());
            break;
          case 6:
            AutoRouter.of(context).push(RequestNurseRoute());
            break;
        }
      },
      child: Container(
        margin: const EdgeInsets.all(5),
        padding: const EdgeInsets.all(8),
        height: 90,
        width: 140,
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.greyWhite.withOpacity(.07)),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: [
            Container(
                decoration: BoxDecoration(
                    color: model.color,
                    borderRadius: BorderRadius.circular(15)),
                height: 100,
                width: double.infinity,
                child: Image.asset(model.image)),
            // MyText(title: "image", color: MyColors.black, size: 11),
            SizedBox(
              height: 3,
            ),
            MyText(title: model.title, color: MyColors.black, size: 11),
          ],
        ),
      ),
    );
  }
}
