part of 'see_all_imports.dart';

class SeeAllFeatures extends StatefulWidget {
  final String title;
  final bool showFilter;
  final List<FeaturesModel>? model;

  const SeeAllFeatures({Key? key, required this.title,required this.showFilter, this.model}) : super(key: key);
  @override
  State<SeeAllFeatures> createState() => _SeeAllFeaturesState();
}

class _SeeAllFeaturesState extends State<SeeAllFeatures> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(
        title: widget.title,
        isCenter: true,
      ),
      body: Column(
        children: [
          BuildSearch(),
          Expanded(
            child: GridView.builder(
              itemCount: widget.model!.length,
              itemBuilder: (_, index) {
                return SeeAllFeaturesContainer(model: widget.model![index],);
              },
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 5.0,
                mainAxisSpacing: 5.0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
