import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/tf_validator.dart';

import '../../../../general/constants/MyColors.dart';
import '../medical_record_imports.dart';




part 'build_medical_body.dart';
part 'build_medical_search.dart';