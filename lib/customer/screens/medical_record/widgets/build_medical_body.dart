part of 'medical_record_widgets_imports.dart';



class BuildMedicalBody extends StatelessWidget {
  final List<String> model;
  const BuildMedicalBody({Key? key,required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(child: ListView.builder(itemCount: model.length,itemBuilder: (_,index){
      return Container(
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: MyColors.greyWhite.withOpacity(.2),),
        ),
        margin: const EdgeInsets.symmetric(horizontal: 20,vertical: 5),
        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 8),
        child: Row(
          children: [
            Image.asset(Res.pdfRecord,height: 50,),
            const SizedBox(width: 15,),
            MyText(title: model[index], color: MyColors.black, size: 12,),
          ],
        ),
      );
    }));
  }
}
