part of 'medical_record_widgets_imports.dart';



class BuildMedicalSearch extends StatelessWidget {
  final MedicalRecordData medicalRecordData;
  const BuildMedicalSearch({Key? key,required this.medicalRecordData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width*.85,
      child: GenericTextField(
        fillColor: MyColors.white,
        fieldTypes: FieldTypes.normal,
        hint: "Search",
        // hint: tr(context, "search"),
        hintColor: MyColors.greyWhite,
        prefixIcon: Icon(Icons.search_rounded,color: MyColors.greyWhite,),
        suffixIcon: Icon(Icons.keyboard_voice_rounded,color: MyColors.greyWhite,),
        onSubmit: () {
          medicalRecordData.searchController.clear();
        },
        controller: medicalRecordData.searchController,
        margin:
        const EdgeInsets.symmetric(vertical: 7, horizontal: 10),
        action: TextInputAction.done,
        type: TextInputType.text,
        validate: (value) => value!.validateEmpty(context),
      ),
    );
  }
}
