part of 'medical_record_imports.dart';

class MedicalRecord extends StatefulWidget {
  const MedicalRecord({Key? key}) : super(key: key);

  @override
  State<MedicalRecord> createState() => _MedicalRecordState();
}

class _MedicalRecordState extends State<MedicalRecord>
    with TickerProviderStateMixin {
  final MedicalRecordData medicalRecordData = MedicalRecordData();
  final List<String> medicalRecordList = [
    'Blood Test',
    'Suger Test',
    'Liver Test',
    'DNA Test',
    'Hormone Test',
    'Albumin Test',
    'Albumin Test',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(
        title: 'Medical Record',
        isCenter: true,
        actions: [
          IconButton(
            icon: Icon(
              Icons.filter_list_rounded,
              color: MyColors.primary,
            ),
            onPressed: () {
              Nav.navigateToButtonSheet(context, this, Filter(),
                  sheetHeight: 250);
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Row(
            children: [
              BuildMedicalSearch(
                medicalRecordData: medicalRecordData,
              ),
              Container(
                decoration: BoxDecoration(
                  color: MyColors.primary,
                  borderRadius: BorderRadius.circular(5),
                ),
                width: 40,
                child: Image.asset(
                  Res.plus,
                  color: MyColors.white,
                ),
              ),
            ],
          ),
          BuildMedicalBody(model: medicalRecordList,),
        ],
      ),
    );
  }
}
