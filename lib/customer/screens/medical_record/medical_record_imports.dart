import 'package:base_flutter/customer/screens/medical_record/widgets/medical_record_widgets_imports.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

import '../../../general/constants/MyColors.dart';
import '../../../general/widgets/DefaultAppBar.dart';
import '../../../general/widgets/Navigator.dart';
import '../filter/filter_imports.dart';



part 'medical_record.dart';
part 'medical_record_data.dart';