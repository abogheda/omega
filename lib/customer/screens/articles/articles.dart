part of 'articles_imports.dart';



class Articles extends StatelessWidget {
  final List articleList = articlesList;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(
        title: 'Articles',
        isCenter: true,
      ),
      body: GridView.builder(
              itemCount: articleList.length,
              itemBuilder: (_, index) {
                return ArticlesItem(model: articleList[index],);
              },
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 3.0,
                mainAxisSpacing: 5.0,
              ),
            ),
          );
  }
}
