part of 'articles_widgets_imports.dart';



class ArticlesItem extends StatelessWidget {
  final ArticleModel model;
  const ArticlesItem({Key? key,required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 7,vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 7,vertical: 5),
        decoration: BoxDecoration(
            // color: Colors.black,
            color: model.color,
            borderRadius: BorderRadius.circular(15)),
        height: 100,
        width: double.infinity,

        child:           Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            MyText(title: model.title, color: MyColors.black, size: 10,fontWeight: FontWeight.w600,),
            Expanded(child: MyText(title: model.site, color: MyColors.black, size: 9)),

          ],
        ),
    );
  }
}
