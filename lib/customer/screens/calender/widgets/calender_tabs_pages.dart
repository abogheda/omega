part of'calender_widgets_imports.dart';

class CalenderTabsPages extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        children: [
          AppointmentsScreen(),
          Tests(),
          Drugs(),

        ],
      ),
    );
  }
}
