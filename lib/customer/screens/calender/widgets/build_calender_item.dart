part of 'calender_widgets_imports.dart';


class CalenderItem extends StatelessWidget {
  final CalenderModel? calenderModel;
  const CalenderItem({Key? key, this.calenderModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85,
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(color: MyColors.greyWhite),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ListTile(
            leading: CircleAvatar(
              radius: 30,
              child: Image.asset(calenderModel?.image ?? "",height: 35,),
            ),
            title: MyText(
                title: calenderModel?.testName ?? "",
                color: MyColors.black,
                size: 12),
            subtitle: MyText(
                title: calenderModel?.date ?? "",
                color: MyColors.grey,
                size: 10),
          ),
        ],
      ),
    );
  }
}
