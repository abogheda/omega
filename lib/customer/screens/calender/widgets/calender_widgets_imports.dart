import 'package:base_flutter/customer/models/calender_model.dart';
import 'package:base_flutter/customer/screens/calender/calender_imports.dart';
import 'package:base_flutter/customer/screens/calender/tabs/appointments/appointments_imports.dart';
import 'package:base_flutter/customer/screens/calender/tabs/drugs/drugs_imports.dart';
import 'package:base_flutter/customer/screens/calender/tabs/tests/tests_imports.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../general/constants/MyColors.dart';





part 'calender_tabs_item.dart';
part 'calender_tabs_pages.dart';
part 'calender_tabs_view.dart';
part 'build_calender_item.dart';