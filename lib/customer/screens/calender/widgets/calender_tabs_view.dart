part of'calender_widgets_imports.dart';
class CalenderTabsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: CalenderData().tabsCubit,
      builder: (_, state) {
        return Container(
          margin: const EdgeInsets.symmetric(vertical: 15,horizontal: 30),
          child: TabBar(
            indicatorColor: Colors.transparent,
            onTap: (value) => CalenderData().tabsCubit.onUpdateData(value),
            tabs: [
              CalenderTabsItem(title: "Appointments", index: 0, current: state.data),
              CalenderTabsItem(title: "Tests", index: 1, current: state.data),
              CalenderTabsItem(title: "Drugs", index: 2, current: state.data),
            ],
          ),
        );
      },
    );
  }
}
