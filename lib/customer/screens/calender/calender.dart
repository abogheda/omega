part of 'calender_imports.dart';


class Calender extends StatelessWidget {
  const Calender({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: MyColors.white,
        appBar: DefaultAppBar(title: "Calender",isCenter: true,),
        body: Column(
          children: [
            CalenderTabsView(),
            CalenderTabsPages(),
          ],
        ),
      ),
    );
  }
}
