part of 'drugs_imports.dart';
class Drugs extends StatelessWidget {
  const Drugs({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final List<CalenderModel> drugsList = drugsDummyList;

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          itemCount: drugsList.length,
          itemBuilder: (_, index) {
            return CalenderItem(calenderModel: drugsList[index],);
          }),
    );
  }
}
