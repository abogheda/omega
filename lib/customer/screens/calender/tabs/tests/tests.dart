part of 'tests_imports.dart';
class Tests extends StatelessWidget {
  const Tests({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<CalenderModel> testsList = testsDummyList;

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          itemCount: testsList.length,
          itemBuilder: (_, index) {
            return CalenderItem(calenderModel: testsList[index],);
          }),
    );
  }
}
