part of 'appointments_imports.dart';
class AppointmentsScreen extends StatelessWidget {
  const AppointmentsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<CalenderModel> appointmentsList = appointmentsDummyList;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          itemCount: appointmentsList.length,
          itemBuilder: (_, index) {
            return CalenderItem(calenderModel: appointmentsList[index],);
          }),
    );
  }
}
