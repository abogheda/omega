part of 'calender_imports.dart';


class CalenderData{
  static final CalenderData _instance = CalenderData._internal();

  CalenderData._internal();

  factory CalenderData({bool newInstance = false}) {
    if (newInstance) {
      return CalenderData();
    } else {
      return _instance;
    }
  }
  final GenericBloc<int> tabsCubit = GenericBloc(0);
}