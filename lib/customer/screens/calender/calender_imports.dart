import 'package:base_flutter/customer/screens/calender/widgets/calender_widgets_imports.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';

import '../../../general/constants/MyColors.dart';
import '../../../general/widgets/DefaultAppBar.dart';


part 'calender.dart';
part 'calender_data.dart';