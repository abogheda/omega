part of 'inbody_widgets_imports.dart';

class InbodyInputs extends StatelessWidget {
  final InbodyData inbodyData;
  const InbodyInputs({Key? key, required this.inbodyData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: inbodyData.formKey,
      child: ListView(
        children: [
          DropdownTextField(
            // dropKey: widget.data.workKey,
            margin: const EdgeInsets.symmetric(vertical: 10),
            // onChange: (value) => widget.data.workCubit.onUpdateData(value),
            useName: false,
            data: ["Lose Weight", "Gain Weight","Body Building","Health Care"],
            hint: "ُTarget", validate: (value) => value.noValidate(context),
          ),
          Row(
            children: [
              Expanded(
                child: GenericTextField(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  controller: inbodyData.age,
                  fieldTypes: FieldTypes.normal,
                  hint: "Age",
                  type: TextInputType.number,
                  action: TextInputAction.next,
                  validate: (value) => value!.validateEmpty(context),
                ),
              ),
              Expanded(
                child: GenericTextField(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  fieldTypes: FieldTypes.normal,
                  hint: "Weight",
                  controller: inbodyData.weight,
                  type: TextInputType.number,
                  action: TextInputAction.next,
                  validate: (value) => value!.validateEmpty(context),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GenericTextField(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  controller: inbodyData.waterPercentage,
                  fieldTypes: FieldTypes.normal,
                  hint: "Water Percentage",
                  type: TextInputType.number,
                  action: TextInputAction.next,
                  validate: (value) => value!.validateEmpty(context),
                ),
              ),
              Expanded(
                child: GenericTextField(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  fieldTypes: FieldTypes.normal,
                  hint: "Muscle",
                  controller: inbodyData.muscle,
                  type: TextInputType.number,
                  action: TextInputAction.next,
                  validate: (value) => value!.validateEmpty(context),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: GenericTextField(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  controller: inbodyData.protein,
                  fieldTypes: FieldTypes.normal,
                  hint: "Protein",
                  type: TextInputType.number,
                  action: TextInputAction.next,
                  validate: (value) => value!.validateEmpty(context),
                ),
              ),
              Expanded(
                child: GenericTextField(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  fieldTypes: FieldTypes.normal,
                  hint: "BMI",
                  controller: inbodyData.bmi,
                  type: TextInputType.number,
                  action: TextInputAction.next,
                  validate: (value) => value!.validateEmpty(context),
                ),
              ),
            ],
          ),
          GenericTextField(
            margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            fieldTypes: FieldTypes.normal,
            hint: "Minerals",
            controller: inbodyData.minerals,
            type: TextInputType.number,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
          ),
        ],
      ),
    );
  }
}
