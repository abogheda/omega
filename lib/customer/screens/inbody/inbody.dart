part of 'inbody_imports.dart';


class Inbody extends StatefulWidget {
  const Inbody({Key? key}) : super(key: key);

  @override
  State<Inbody> createState() => _InbodyState();
}

class _InbodyState extends State<Inbody> {
  final InbodyData inbodyData = InbodyData();
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=> FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: MyColors.white,
        appBar: DefaultAppBar(
          title: 'Inbody',
          isCenter: true,
        ),
        body: InbodyInputs(inbodyData: inbodyData,),
        bottomNavigationBar: DefaultButton(onTap: () {inbodyData.calInbody(context);  }, title: 'Continue',),
      ),
    );
  }
}
