part of 'inbody_imports.dart';

class InbodyData {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final TextEditingController age = new TextEditingController();
  final TextEditingController weight = new TextEditingController();
  final TextEditingController waterPercentage = new TextEditingController();
  final TextEditingController muscle = new TextEditingController();
  final TextEditingController protein = new TextEditingController();
  final TextEditingController bmi = new TextEditingController();
  final TextEditingController minerals = new TextEditingController();

  void calInbody(BuildContext context) {
    // if(formKey.currentState!.validate()){
    var rng = Random();
    int randomNum = rng.nextInt(14);
    print("Random Number : $randomNum");
    CustomToast.showSimpleToast(msg: 'The diet is being prepared');
    Future.delayed(Duration(seconds: 2),(){
        CustomToast.showSimpleToast(msg: 'The diet is now ready');
        AutoRouter.of(context).push(InbodyResultRoute(id: randomNum));
      });
    }
  // }
}
