import 'dart:math';

import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/inbody/widgets/inbody_widgets_imports.dart';
import 'package:base_flutter/general/utilities/http/dio/dio_helper.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../general/constants/MyColors.dart';
import '../../../general/widgets/DefaultAppBar.dart';



part 'inbody.dart';
part 'inbody_data.dart';