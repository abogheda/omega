import 'package:base_flutter/customer/models/nurse_details.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/utils_functions/UtilsImports.dart';
import 'package:base_flutter/res.dart';
import 'package:calender_picker/date_picker_widget.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../general/widgets/AuthScaffold.dart';
import '../../dummy_data/nurse_details_dummy.dart';



part 'book_nurse.dart';
part 'book_nurse_data.dart';