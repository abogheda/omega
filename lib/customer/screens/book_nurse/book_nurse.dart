part of 'book_nurse_imports.dart';

class BookNurse extends StatefulWidget {
  final int id;
  const BookNurse({Key? key,required this.id}) : super(key: key);

  @override
  State<BookNurse> createState() => _BookNurseState();
}

class _BookNurseState extends State<BookNurse> {
  final List<NurseDetailsModel> nurseDetails = nurseDetailsDummyList;
  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            // padding: const EdgeInsets.all(5),
            // margin: const EdgeInsets.all(5),
            height: 100,
            child: CalenderPicker(
              DateTime.now(),
              monthTextStyle: TextStyle(
                  color: MyColors.black, backgroundColor: Colors.amber),
              daysCount: 100,
              height: 100,
              width: 50,
              initialSelectedDate: DateTime.now(),
              selectionColor: MyColors.primary,
              selectedTextColor: Colors.white,
              onDateChange: (date) {
                // New date selected
                setState(() {
                  // _selectedValue = date;
                });
              },
            ),
          ),
          SizedBox(
            height: 10,
          ),
          MyText(
            title: "Appointments",
            color: MyColors.black,
            size: 13,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(
            height: 10,
          ),
          Wrap(
            children: List.generate(
                nurseDetails[widget.id-1].times.length,
                (index) => Container(
                      height: 35,
                      margin: const EdgeInsets.all(2),
                      width: 80,
                      decoration: BoxDecoration(
                        color: MyColors.primary,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Center(
                          child: MyText(
                        title: nurseDetails[widget.id-1].times[index],
                        size: 12,
                        color: MyColors.white,
                      )),
                    )),
          ),
          SizedBox(
            height: 10,
          ),
          MyText(
            title: "Fees Information",
            color: MyColors.black,
            size: 13,
            fontWeight: FontWeight.bold,
          ),
          Container(
            height: 100,
            margin: const EdgeInsets.all(5),
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(
                color: MyColors.grey.withOpacity(.1),
              ),
            ),
            child: Row(
              children: [
                Container(height: 70,width: 70,
                  decoration: BoxDecoration(
                    color: MyColors.forth,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Image.asset(Res.home,color: MyColors.primary,),
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MyText(
                      title: "Name",
                      color: MyColors.black,
                      size: 10,
                      fontWeight: FontWeight.bold,
                    ),
                    MyText(
                      title: nurseDetails[widget.id-1].title,
                      color: MyColors.grey,
                      size: 9,
                    ),
                  ],
                ),
              ],
            )
          ),
          InkWell(
            onTap: (){
              Utils.callPhone(phone: nurseDetails[widget.id-1].number);
            },
            child: Container(
              height: 100,
              margin: const EdgeInsets.all(5),
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                  color: MyColors.grey.withOpacity(.1),
                ),
              ),
              child: Row(
                children: [
                  Container(height: 70,width: 70,
                    decoration: BoxDecoration(
                      color: MyColors.forth,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Image.asset(Res.home,color: MyColors.primary,),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      MyText(
                        title: "Phone Number",
                        color: MyColors.black,
                        size: 10,
                        fontWeight: FontWeight.bold,
                      ),
                      MyText(
                        title: nurseDetails[widget.id-1].number,
                        color: MyColors.grey,
                        size: 9,
                      ),
                    ],
                  ),
                ],
              )
            ),
          ),
        ],
      ),
    ));
  }
}
