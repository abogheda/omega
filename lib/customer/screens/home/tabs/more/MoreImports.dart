import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/home/tabs/more/widgets/MoreWidgetsImports.dart';

import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';

import '../../../../../general/widgets/Navigator.dart';
import '../../../change_lang/ChangeLangImports.dart';



part 'MoreData.dart';
part 'More.dart';
