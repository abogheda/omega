part of 'MoreImports.dart';

class More extends StatefulWidget {
  const More({Key? key}) : super(key: key);

  @override
  _MoreState createState() => _MoreState();
}

class _MoreState extends State<More> with TickerProviderStateMixin {
  final MoreData moreData = MoreData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.primary,
      body: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          Stack(
            children: [
              Container(
                // margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                height: MediaQuery.of(context).size.height * .1,
                width: double.infinity,
                decoration: BoxDecoration(color: MyColors.primary),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 6),
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: MyColors.white,
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 50,
                      ),
                      BuildMoreCard(
                        logo: Image.asset(
                          Res.profile,
                          scale: .7,
                        ),
                        title: tr(context, "myAccount"),
                        subTitle: tr(context, "seeAccount"),
                        onTab: () =>
                            AutoRouter.of(context).push(MyAccountRoute()),
                        // onTab: () {},
                      ),
                      BuildMoreCard(
                        logo: Image.asset(
                          Res.lang,
                          scale: 3.3,
                        ),
                        title: tr(context, "lang"),
                        subTitle: tr(context, "controlLang"),
                        onTab: () {
                          Nav.navigateToButtonSheet(context, this, ChangeLanguage());
                        },
                        // onTab: () => AutoRouter.of(context).push(LanguageRoute()),
                      ),
                      BuildMoreCard(
                        logo: Image.asset(
                          Res.sharrot,
                          scale: 4,
                        ),
                        title: tr(context, "terms"),
                        subTitle: tr(context, "seeTerms"),
                        onTab: () => AutoRouter.of(context).push(TermsRoute()),
                      ),
                      BuildMoreCard(
                        logo: Image.asset(
                          Res.about,
                          scale: 1.2,
                        ),
                        title: tr(context, "about"),
                        subTitle: tr(context, "seeAbout"),
                        onTab: () => AutoRouter.of(context).push(AboutRoute()),
                      ),
                      // BuildMoreCard(
                      //     logo: Image.asset(
                      //       Res.phone,
                      //       scale: 4,
                      //     ),
                      //     title: tr(context, "contactUs"),
                      //     subTitle: tr(context, "help"),
                      //     onTab: () =>
                      //         AutoRouter.of(context).push(ContactUsRoute())),
                      BuildMoreCard(
                        logo: Image.asset(
                          Res.logout,
                          scale: 4,
                        ),
                        title: tr(context, "logout"),
                        subTitle: tr(context, "youCanLogOut"),
                        onTab: () => moreData.logOut(context),
                      )
                    ],
                  ),
                ),
              ),
              Positioned(
                top: MediaQuery.of(context).size.height * .08,
                // right: MediaQuery.of(context).size.width / 1.5,
                left: MediaQuery.of(context).size.width / 2.71,
                child: CachedImage(
                  haveRadius: true,
                  borderRadius: BorderRadius.circular(100),
                  height: 100,
                  width: 100,
                  url:
                      "https://hips.hearstapps.com/hmg-prod/images/portrait-of-a-happy-young-doctor-in-his-clinic-royalty-free-image-1661432441.jpg?crop=0.66698xw:1xh;center,top&resize=1200:*",
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
