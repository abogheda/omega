part of 'MainPageWidgetsImports.dart';

class BuildDoctorsItem extends StatelessWidget {
  final List<FeaturesModel> model;
  const BuildDoctorsItem({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 192,
      child: ListView.builder(
        itemCount: model.length,
        itemBuilder: (_, index) {
          return Container(
            margin: const EdgeInsets.all(5),
            padding: const EdgeInsets.all(8),
            height: 90,
            width: 140,
            decoration: BoxDecoration(
                border: Border.all(color: MyColors.greyWhite.withOpacity(.07)),
                borderRadius: BorderRadius.circular(15)),
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: model[index].color,
          borderRadius: BorderRadius.circular(15)),
                    height: 135,
                    width: double.infinity,

                    child: Image.asset(model[index].image)),
                // MyText(title: "image", color: MyColors.black, size: 11),
                SizedBox(
                  height: 3,
                ),
                MyText(title: model[index].title, color: MyColors.black, size: 11),
              ],
            ),
          );
        },
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
