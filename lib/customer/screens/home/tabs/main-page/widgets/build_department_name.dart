part of 'MainPageWidgetsImports.dart';


class BuildDepartmentName extends StatelessWidget {
  final String departmentName;
  final bool showFilter;
  final List<FeaturesModel> model;
  const BuildDepartmentName({Key? key,required this.departmentName,required this.showFilter,required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,

        children: [
          InkWell(
            onTap: ()=> AutoRouter.of(context).push(SeeAllFeaturesRoute(title: departmentName, showFilter: showFilter,model:model )),
            child: Row(
              children: [
                Icon(Icons.arrow_back_ios,color: MyColors.primary,),
                MyText(title: "See all", color: MyColors.primary, size: 11),
              ],
            ),
          ),
          MyText(title: "$departmentName", color: MyColors.black, size: 12),

        ],
      ),
    );
  }
}
