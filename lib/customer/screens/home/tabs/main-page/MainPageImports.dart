
import 'package:base_flutter/customer/models/features_model.dart';
import 'package:base_flutter/customer/screens/filter/filter_imports.dart';
import 'package:base_flutter/customer/screens/home/tabs/main-page/widgets/MainPageWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';

import '../../../../../general/widgets/Navigator.dart';
import '../../../../dummy_data/doctor_dummy.dart';
import '../../../../dummy_data/feature_dummy.dart';
import '../../../../dummy_data/nurse_dummy.dart';

part 'MainPage.dart';
part 'MainPageData.dart';
