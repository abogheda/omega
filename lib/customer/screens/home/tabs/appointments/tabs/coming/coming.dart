part of 'coming_imports.dart';

class Coming extends StatelessWidget {
  const Coming({Key? key}) : super(key: key);
  Widget build(BuildContext context) {
    final List<AppointmentModel> comingList = comingAppointmentDummyList;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          itemCount: comingList.length,
          itemBuilder: (_, index) {
            return Container(
              height: 120,
              margin: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: MyColors.greyWhite),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      radius: 30,
                      child: Image.asset(comingList[index].image,height: 45,)
                    ),
                    title: MyText(
                        title: comingList[index].testName,
                        color: MyColors.black,
                        size: 12),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                            title: comingList[index].date,
                            color: MyColors.black,
                            size: 12),
                        MyText(
                            title: comingList[index].time!,
                            color: MyColors.grey,
                            size: 12),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: MyText(
                        title: "${comingList[index].price}\$", color: MyColors.primary, size: 12),
                  )
                ],
              ),
            );
          }),
    );
  }
}
