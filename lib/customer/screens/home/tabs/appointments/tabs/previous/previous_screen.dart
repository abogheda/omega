part of 'previous_imports.dart';

class PreviousScreen extends StatelessWidget {
  const PreviousScreen({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final List<AppointmentModel> previousList = previousAppointmentDummyList;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body:  ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          itemCount: previousList.length,
          itemBuilder: (_, index) {
            return Container(
              height: 120,
              margin: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: MyColors.greyWhite),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      radius: 30,
                        child: Image.asset(previousList[index].image,height: 45,)

            ),
                    title: MyText(
                        title: previousAppointmentDummyList[index].testName,
                        color: MyColors.black,
                        size: 12),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                            title: previousAppointmentDummyList[index].date,
                            color: MyColors.black,
                            size: 12),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: MyText(
                        title: "${previousAppointmentDummyList[index].price}\$", color: Colors.red, size: 12),
                  )
                ],
              ),
            );
          }),
    );
  }
}
