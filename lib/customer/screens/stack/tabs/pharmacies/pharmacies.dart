part of 'pharmacies_imports.dart';
class Pharmacies extends StatelessWidget {
  const Pharmacies({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final   List<StackModel> pharmacyList = stackPharmacyDummyList;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body:  ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          itemCount: pharmacyList.length,
          itemBuilder: (_, index) {
            return BuildTabScreenItem(stackModel: pharmacyList[index],);
          }),
    );
  }
}
