part of 'doctors_imports.dart';
class Doctors extends StatelessWidget {
  const Doctors({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final   List<StackModel> doctorList = stackDoctorDummyList;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          itemCount: doctorList.length,
          itemBuilder: (_, index) {
            return BuildTabScreenItem(stackModel: doctorList[index],);
          }),
    );
  }
}
