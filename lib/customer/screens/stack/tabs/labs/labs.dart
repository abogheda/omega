part of 'labs_imports.dart';
class Labs extends StatelessWidget {
  const Labs({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final   List<StackModel> labsList = stackLabsDummyList;

    return Scaffold(
      backgroundColor: Colors.transparent,
      body:  ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          itemCount: labsList.length,
          itemBuilder: (_, index) {
            return BuildTabScreenItem(stackModel: labsList[index],);
          }),
    );
  }
}
