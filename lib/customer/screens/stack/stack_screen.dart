part of 'stack_screen_imports.dart';

class StackScreen extends StatefulWidget {
  const StackScreen({Key? key}) : super(key: key);

  @override
  State<StackScreen> createState() => _StackScreenState();
}

class _StackScreenState extends State<StackScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: MyColors.white,
        appBar: DefaultAppBar(title: "Stack",isCenter: true,),
        body: Column(
          children: [
            BuildStackTabsView(),
            BuildTabsPages(),
          ],
        ),
      ),
    );
  }
}
