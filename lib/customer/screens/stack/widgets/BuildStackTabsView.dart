part of 'stack_widgets_imports.dart';
class BuildStackTabsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: StackScreenData().tabsCubit,
      builder: (_, state) {
        return Container(
          margin: const EdgeInsets.symmetric(vertical: 15,horizontal: 30),
          child: TabBar(
            indicatorColor: Colors.transparent,
            onTap: (value) => StackScreenData().tabsCubit.onUpdateData(value),
            tabs: [
              BuildTabsItem(title: "Doctors", index: 0, current: state.data),
              BuildTabsItem(title: "Pharmacies", index: 1, current: state.data),
              BuildTabsItem(title: "Labs", index: 2, current: state.data),
            ],
          ),
        );
      },
    );
  }
}
