part of 'stack_widgets_imports.dart';


class BuildTabScreenItem extends StatelessWidget {
  final StackModel? stackModel;
  const BuildTabScreenItem({Key? key, this.stackModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 135,
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(color: MyColors.greyWhite),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ListTile(
            leading: CircleAvatar(
              radius: 30,
              child: Image.asset(stackModel?.image ?? "",height: 40,)
            ),
            title: MyText(
                title: stackModel?.doctorName ?? "",
                color: MyColors.black,
                size: 12),
            subtitle: Row(
              children: [
                Icon(Icons.location_on,color: MyColors.grey.withOpacity(.5),size: 20,),
                MyText(
                    title: stackModel?.location ?? "",
                    color: MyColors.grey.withOpacity(.5),
                    size: 12),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Icon(Icons.star,color: Colors.amber,size: 20,),
                MyText(
                    title: stackModel?.rate ?? "",
                    color: MyColors.black,
                    size: 10),Icon(Icons.location_on,color: Colors.amber,size: 20,),
                MyText(
                    title: stackModel?.distance ?? "",
                    color: MyColors.black,
                    size: 10),
              ],
            ),
          )
        ],
      ),
    );
  }
}
