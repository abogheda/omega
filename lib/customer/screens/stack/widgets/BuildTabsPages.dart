part of 'stack_widgets_imports.dart';

class BuildTabsPages extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        children: [
          Doctors(),
          Pharmacies(),
          Labs(),

        ],
      ),
    );
  }
}
