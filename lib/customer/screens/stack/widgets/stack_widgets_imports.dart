import 'package:base_flutter/customer/models/stack_model.dart';
import 'package:base_flutter/customer/screens/stack/stack_screen_imports.dart';
import 'package:base_flutter/customer/screens/stack/tabs/labs/labs_imports.dart';
import 'package:base_flutter/customer/screens/stack/tabs/pharmacies/pharmacies_imports.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../general/constants/MyColors.dart';
import '../tabs/doctors/doctors_imports.dart';


part 'BuildTabsItem.dart';
part 'BuildTabsPages.dart';
part 'BuildStackTabsView.dart';
part 'build_tab_screen_item.dart';