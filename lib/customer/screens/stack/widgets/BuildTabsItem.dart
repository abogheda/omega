part of 'stack_widgets_imports.dart';
class BuildTabsItem extends StatelessWidget {
final String title;
final int index;
final int current;

  const BuildTabsItem({required this.title,required this.index,required this.current});
  @override
  Widget build(BuildContext context) {
    return Tab(
      iconMargin:const EdgeInsets.all(0) ,
      child: Container(
        // padding: const EdgeInsets.all(10),
        // margin: const EdgeInsets.all(3),
        height: 40,

        alignment: Alignment.center,
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: index == current ? MyColors.primary :MyColors.grey )),
        child: MyText(
          title: title,
          size: 9,
          color: index == current ? MyColors.primary : MyColors.grey,
        ),
      ),
    );
  }
}
