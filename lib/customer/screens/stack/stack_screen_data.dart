part of 'stack_screen_imports.dart';

class StackScreenData {
  static final StackScreenData _instance = StackScreenData._internal();

  StackScreenData._internal();

  factory StackScreenData({bool newInstance = false}) {
    if (newInstance) {
      return StackScreenData();
    } else {
      return _instance;
    }
  }
  final GenericBloc<int> tabsCubit = GenericBloc(0);
}
