import 'package:base_flutter/customer/screens/stack/widgets/stack_widgets_imports.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';

import '../../../general/constants/MyColors.dart';
import '../../../general/widgets/DefaultAppBar.dart';



part 'stack_screen.dart';
part 'stack_screen_data.dart';