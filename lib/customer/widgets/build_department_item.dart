import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/models/features_model.dart';
import 'package:base_flutter/customer/models/nurse_model.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../general/constants/MyColors.dart';
import '../../general/utilities/routers/RouterImports.gr.dart';

class BuildDepartmentItem extends StatelessWidget {
  final List<NurseModel> model;
  const BuildDepartmentItem({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      child: Wrap(
        children: List.generate(model.length, (index) => InkWell(
          onTap: (){
            AutoRouter.of(context).push(BookNurseRoute(id: model[index].id));
          },
          child: Container(
            // margin: const EdgeInsets.all(5),
            padding: const EdgeInsets.all(8),
            height: 150,
            width: 120,
            decoration: BoxDecoration(
                border:
                Border.all(color: MyColors.greyWhite.withOpacity(.07)),
                borderRadius: BorderRadius.circular(15)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    decoration: BoxDecoration(
                        color: model[index].color,
                        borderRadius: BorderRadius.circular(15)),
                    height: 75,
                    width: double.infinity,
                    child: Image.asset(model[index].image)),
                // MyText(title: "image", color: MyColors.black, size: 11),
                SizedBox(
                  height: 3,
                ),
                MyText(
                    title: model[index].title,
                    color: MyColors.black,
                    size: 11),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                      size: 15,
                    ),
                    MyText(title: model[index].rate, color: MyColors.black, size: 10),
                    Icon(
                      Icons.location_on,
                      color: Colors.amber,
                      size: 15,
                    ),
                    MyText(title: model[index].distance, color: MyColors.black, size: 10),
                  ],
                ),
              ],
            ),
          ),
        ),),
      ),
    );
  }
}
