import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/models/features_model.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../general/constants/MyColors.dart';



class DepartmentContainer extends StatelessWidget {
   final FeaturesModel model;

  const DepartmentContainer({Key? key,required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        switch(model.id){
          case 3 :
             AutoRouter.of(context).push(CalenderRoute());
             break;case 4 :
             AutoRouter.of(context).push(StackScreenRoute());
             break;
             case 5 :
             AutoRouter.of(context).push(MedicalRecordRoute());
             break;
             case 2 :
             AutoRouter.of(context).push(ArticlesRoute());
             break;
        }
      },
      child: Container(
        margin: const EdgeInsets.all(5),
        padding: const EdgeInsets.all(8),
        height: 90,
        width: 140,
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.greyWhite.withOpacity(.07)),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: [
            Container(
                decoration: BoxDecoration(
                    color: model.color,
                    borderRadius: BorderRadius.circular(15)),
                height: 100,
                width: double.infinity,

                child: Image.asset(model.image)),
            // MyText(title: "image", color: MyColors.black, size: 11),
            SizedBox(
              height: 3,
            ),
            MyText(title: model.title, color: MyColors.black, size: 11),
          ],
        ),
      ),
    );
  }
}
