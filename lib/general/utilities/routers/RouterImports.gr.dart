// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i29;
import 'package:base_flutter/customer/models/features_model.dart' as _i31;
import 'package:base_flutter/customer/screens/articles/articles_imports.dart'
    as _i22;
import 'package:base_flutter/customer/screens/book_nurse/book_nurse_imports.dart'
    as _i26;
import 'package:base_flutter/customer/screens/calender/calender_imports.dart'
    as _i24;
import 'package:base_flutter/customer/screens/home/HomeImports.dart' as _i17;
import 'package:base_flutter/customer/screens/inbody/inbody_imports.dart'
    as _i27;
import 'package:base_flutter/customer/screens/inbody_result/inbody_result_imports.dart'
    as _i28;
import 'package:base_flutter/customer/screens/medical_record/medical_record_imports.dart'
    as _i21;
import 'package:base_flutter/customer/screens/my_account/my_account_imports.dart'
    as _i20;
import 'package:base_flutter/customer/screens/register/register_imports.dart'
    as _i15;
import 'package:base_flutter/customer/screens/request_nurse/request_nurse_imports.dart'
    as _i25;
import 'package:base_flutter/customer/screens/see_all_doctors/see_all_doctors_imports.dart'
    as _i19;
import 'package:base_flutter/customer/screens/see_all_features/see_all_imports.dart'
    as _i18;
import 'package:base_flutter/customer/screens/stack/stack_screen_imports.dart'
    as _i23;
import 'package:base_flutter/customer/screens/welcome_page/WelcomePageImports.dart'
    as _i16;
import 'package:base_flutter/general/screens/about/AboutImports.dart' as _i8;
import 'package:base_flutter/general/screens/active_account/ActiveAccountImports.dart'
    as _i4;
import 'package:base_flutter/general/screens/change_password/ChangePasswordImports.dart'
    as _i12;
import 'package:base_flutter/general/screens/confirm_password/ConfirmPasswordImports.dart'
    as _i11;
import 'package:base_flutter/general/screens/contact_us/ContactUsImports.dart'
    as _i9;
import 'package:base_flutter/general/screens/forget_password/ForgetPasswordImports.dart'
    as _i3;
import 'package:base_flutter/general/screens/image_zoom/ImageZoom.dart' as _i13;
import 'package:base_flutter/general/screens/location_address/LocationAddressImports.dart'
    as _i14;
import 'package:base_flutter/general/screens/login/LoginImports.dart' as _i2;
import 'package:base_flutter/general/screens/reset_password/ResetPasswordImports.dart'
    as _i5;
import 'package:base_flutter/general/screens/select_lang/SelectLangImports.dart'
    as _i6;
import 'package:base_flutter/general/screens/select_user/SelectUserImports.dart'
    as _i10;
import 'package:base_flutter/general/screens/splash/SplashImports.dart' as _i1;
import 'package:base_flutter/general/screens/terms/TermsImports.dart' as _i7;
import 'package:flutter/material.dart' as _i30;

class AppRouter extends _i29.RootStackRouter {
  AppRouter([_i30.GlobalKey<_i30.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i29.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) {
      final args = routeData.argsAs<SplashRouteArgs>();
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i1.Splash(navigatorKey: args.navigatorKey));
    },
    LoginRoute.name: (routeData) {
      return _i29.CustomPage<dynamic>(
          routeData: routeData,
          child: _i2.Login(),
          opaque: true,
          barrierDismissible: false);
    },
    ForgetPasswordRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i3.ForgetPassword());
    },
    ActiveAccountRoute.name: (routeData) {
      final args = routeData.argsAs<ActiveAccountRouteArgs>();
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i4.ActiveAccount(userId: args.userId));
    },
    ResetPasswordRoute.name: (routeData) {
      final args = routeData.argsAs<ResetPasswordRouteArgs>();
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i5.ResetPassword(userId: args.userId));
    },
    SelectLangRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i6.SelectLang());
    },
    TermsRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i7.Terms());
    },
    AboutRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i8.About());
    },
    ContactUsRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i9.ContactUs());
    },
    SelectUserRoute.name: (routeData) {
      return _i29.CustomPage<dynamic>(
          routeData: routeData,
          child: _i10.SelectUser(),
          transitionsBuilder: _i29.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 1500,
          opaque: true,
          barrierDismissible: false);
    },
    ConfirmPasswordRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i11.ConfirmPassword());
    },
    ChangePasswordRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i12.ChangePassword());
    },
    ImageZoomRoute.name: (routeData) {
      final args = routeData.argsAs<ImageZoomRouteArgs>();
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i13.ImageZoom(images: args.images));
    },
    LocationAddressRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i14.LocationAddress());
    },
    RegisterRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i15.Register());
    },
    WelcomePageRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i16.WelcomePage());
    },
    HomeRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i17.Home());
    },
    SeeAllFeaturesRoute.name: (routeData) {
      final args = routeData.argsAs<SeeAllFeaturesRouteArgs>();
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i18.SeeAllFeatures(
              key: args.key,
              title: args.title,
              showFilter: args.showFilter,
              model: args.model));
    },
    SeeAllDoctorsRoute.name: (routeData) {
      final args = routeData.argsAs<SeeAllDoctorsRouteArgs>();
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i19.SeeAllDoctors(
              key: args.key,
              title: args.title,
              showFilter: args.showFilter,
              model: args.model));
    },
    MyAccountRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i20.MyAccount());
    },
    MedicalRecordRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i21.MedicalRecord());
    },
    ArticlesRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: _i22.Articles());
    },
    StackScreenRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i23.StackScreen());
    },
    CalenderRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i24.Calender());
    },
    RequestNurseRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i25.RequestNurse());
    },
    BookNurseRoute.name: (routeData) {
      final args = routeData.argsAs<BookNurseRouteArgs>();
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i26.BookNurse(key: args.key, id: args.id));
    },
    InbodyRoute.name: (routeData) {
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i27.Inbody());
    },
    InbodyResultRoute.name: (routeData) {
      final args = routeData.argsAs<InbodyResultRouteArgs>();
      return _i29.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i28.InbodyResult(key: args.key, id: args.id));
    }
  };

  @override
  List<_i29.RouteConfig> get routes => [
        _i29.RouteConfig(SplashRoute.name, path: '/'),
        _i29.RouteConfig(LoginRoute.name, path: '/Login'),
        _i29.RouteConfig(ForgetPasswordRoute.name, path: '/forget-password'),
        _i29.RouteConfig(ActiveAccountRoute.name, path: '/active-account'),
        _i29.RouteConfig(ResetPasswordRoute.name, path: '/reset-password'),
        _i29.RouteConfig(SelectLangRoute.name, path: '/select-lang'),
        _i29.RouteConfig(TermsRoute.name, path: '/Terms'),
        _i29.RouteConfig(AboutRoute.name, path: '/About'),
        _i29.RouteConfig(ContactUsRoute.name, path: '/contact-us'),
        _i29.RouteConfig(SelectUserRoute.name, path: '/select-user'),
        _i29.RouteConfig(ConfirmPasswordRoute.name, path: '/confirm-password'),
        _i29.RouteConfig(ChangePasswordRoute.name, path: '/change-password'),
        _i29.RouteConfig(ImageZoomRoute.name, path: '/image-zoom'),
        _i29.RouteConfig(LocationAddressRoute.name, path: '/location-address'),
        _i29.RouteConfig(RegisterRoute.name, path: '/Register'),
        _i29.RouteConfig(WelcomePageRoute.name, path: '/welcome-page'),
        _i29.RouteConfig(HomeRoute.name, path: '/Home'),
        _i29.RouteConfig(SeeAllFeaturesRoute.name, path: '/see-all-features'),
        _i29.RouteConfig(SeeAllDoctorsRoute.name, path: '/see-all-doctors'),
        _i29.RouteConfig(MyAccountRoute.name, path: '/my-account'),
        _i29.RouteConfig(MedicalRecordRoute.name, path: '/medical-record'),
        _i29.RouteConfig(ArticlesRoute.name, path: '/Articles'),
        _i29.RouteConfig(StackScreenRoute.name, path: '/stack-screen'),
        _i29.RouteConfig(CalenderRoute.name, path: '/Calender'),
        _i29.RouteConfig(RequestNurseRoute.name, path: '/request-nurse'),
        _i29.RouteConfig(BookNurseRoute.name, path: '/book-nurse'),
        _i29.RouteConfig(InbodyRoute.name, path: '/Inbody'),
        _i29.RouteConfig(InbodyResultRoute.name, path: '/inbody-result')
      ];
}

/// generated route for
/// [_i1.Splash]
class SplashRoute extends _i29.PageRouteInfo<SplashRouteArgs> {
  SplashRoute({required _i30.GlobalKey<_i30.NavigatorState> navigatorKey})
      : super(SplashRoute.name,
            path: '/', args: SplashRouteArgs(navigatorKey: navigatorKey));

  static const String name = 'SplashRoute';
}

class SplashRouteArgs {
  const SplashRouteArgs({required this.navigatorKey});

  final _i30.GlobalKey<_i30.NavigatorState> navigatorKey;

  @override
  String toString() {
    return 'SplashRouteArgs{navigatorKey: $navigatorKey}';
  }
}

/// generated route for
/// [_i2.Login]
class LoginRoute extends _i29.PageRouteInfo<void> {
  const LoginRoute() : super(LoginRoute.name, path: '/Login');

  static const String name = 'LoginRoute';
}

/// generated route for
/// [_i3.ForgetPassword]
class ForgetPasswordRoute extends _i29.PageRouteInfo<void> {
  const ForgetPasswordRoute()
      : super(ForgetPasswordRoute.name, path: '/forget-password');

  static const String name = 'ForgetPasswordRoute';
}

/// generated route for
/// [_i4.ActiveAccount]
class ActiveAccountRoute extends _i29.PageRouteInfo<ActiveAccountRouteArgs> {
  ActiveAccountRoute({required String userId})
      : super(ActiveAccountRoute.name,
            path: '/active-account',
            args: ActiveAccountRouteArgs(userId: userId));

  static const String name = 'ActiveAccountRoute';
}

class ActiveAccountRouteArgs {
  const ActiveAccountRouteArgs({required this.userId});

  final String userId;

  @override
  String toString() {
    return 'ActiveAccountRouteArgs{userId: $userId}';
  }
}

/// generated route for
/// [_i5.ResetPassword]
class ResetPasswordRoute extends _i29.PageRouteInfo<ResetPasswordRouteArgs> {
  ResetPasswordRoute({required String userId})
      : super(ResetPasswordRoute.name,
            path: '/reset-password',
            args: ResetPasswordRouteArgs(userId: userId));

  static const String name = 'ResetPasswordRoute';
}

class ResetPasswordRouteArgs {
  const ResetPasswordRouteArgs({required this.userId});

  final String userId;

  @override
  String toString() {
    return 'ResetPasswordRouteArgs{userId: $userId}';
  }
}

/// generated route for
/// [_i6.SelectLang]
class SelectLangRoute extends _i29.PageRouteInfo<void> {
  const SelectLangRoute() : super(SelectLangRoute.name, path: '/select-lang');

  static const String name = 'SelectLangRoute';
}

/// generated route for
/// [_i7.Terms]
class TermsRoute extends _i29.PageRouteInfo<void> {
  const TermsRoute() : super(TermsRoute.name, path: '/Terms');

  static const String name = 'TermsRoute';
}

/// generated route for
/// [_i8.About]
class AboutRoute extends _i29.PageRouteInfo<void> {
  const AboutRoute() : super(AboutRoute.name, path: '/About');

  static const String name = 'AboutRoute';
}

/// generated route for
/// [_i9.ContactUs]
class ContactUsRoute extends _i29.PageRouteInfo<void> {
  const ContactUsRoute() : super(ContactUsRoute.name, path: '/contact-us');

  static const String name = 'ContactUsRoute';
}

/// generated route for
/// [_i10.SelectUser]
class SelectUserRoute extends _i29.PageRouteInfo<void> {
  const SelectUserRoute() : super(SelectUserRoute.name, path: '/select-user');

  static const String name = 'SelectUserRoute';
}

/// generated route for
/// [_i11.ConfirmPassword]
class ConfirmPasswordRoute extends _i29.PageRouteInfo<void> {
  const ConfirmPasswordRoute()
      : super(ConfirmPasswordRoute.name, path: '/confirm-password');

  static const String name = 'ConfirmPasswordRoute';
}

/// generated route for
/// [_i12.ChangePassword]
class ChangePasswordRoute extends _i29.PageRouteInfo<void> {
  const ChangePasswordRoute()
      : super(ChangePasswordRoute.name, path: '/change-password');

  static const String name = 'ChangePasswordRoute';
}

/// generated route for
/// [_i13.ImageZoom]
class ImageZoomRoute extends _i29.PageRouteInfo<ImageZoomRouteArgs> {
  ImageZoomRoute({required List<dynamic> images})
      : super(ImageZoomRoute.name,
            path: '/image-zoom', args: ImageZoomRouteArgs(images: images));

  static const String name = 'ImageZoomRoute';
}

class ImageZoomRouteArgs {
  const ImageZoomRouteArgs({required this.images});

  final List<dynamic> images;

  @override
  String toString() {
    return 'ImageZoomRouteArgs{images: $images}';
  }
}

/// generated route for
/// [_i14.LocationAddress]
class LocationAddressRoute extends _i29.PageRouteInfo<void> {
  const LocationAddressRoute()
      : super(LocationAddressRoute.name, path: '/location-address');

  static const String name = 'LocationAddressRoute';
}

/// generated route for
/// [_i15.Register]
class RegisterRoute extends _i29.PageRouteInfo<void> {
  const RegisterRoute() : super(RegisterRoute.name, path: '/Register');

  static const String name = 'RegisterRoute';
}

/// generated route for
/// [_i16.WelcomePage]
class WelcomePageRoute extends _i29.PageRouteInfo<void> {
  const WelcomePageRoute()
      : super(WelcomePageRoute.name, path: '/welcome-page');

  static const String name = 'WelcomePageRoute';
}

/// generated route for
/// [_i17.Home]
class HomeRoute extends _i29.PageRouteInfo<void> {
  const HomeRoute() : super(HomeRoute.name, path: '/Home');

  static const String name = 'HomeRoute';
}

/// generated route for
/// [_i18.SeeAllFeatures]
class SeeAllFeaturesRoute extends _i29.PageRouteInfo<SeeAllFeaturesRouteArgs> {
  SeeAllFeaturesRoute(
      {_i30.Key? key,
      required String title,
      required bool showFilter,
      List<_i31.FeaturesModel>? model})
      : super(SeeAllFeaturesRoute.name,
            path: '/see-all-features',
            args: SeeAllFeaturesRouteArgs(
                key: key, title: title, showFilter: showFilter, model: model));

  static const String name = 'SeeAllFeaturesRoute';
}

class SeeAllFeaturesRouteArgs {
  const SeeAllFeaturesRouteArgs(
      {this.key, required this.title, required this.showFilter, this.model});

  final _i30.Key? key;

  final String title;

  final bool showFilter;

  final List<_i31.FeaturesModel>? model;

  @override
  String toString() {
    return 'SeeAllFeaturesRouteArgs{key: $key, title: $title, showFilter: $showFilter, model: $model}';
  }
}

/// generated route for
/// [_i19.SeeAllDoctors]
class SeeAllDoctorsRoute extends _i29.PageRouteInfo<SeeAllDoctorsRouteArgs> {
  SeeAllDoctorsRoute(
      {_i30.Key? key,
      required String title,
      required bool showFilter,
      required List<_i31.FeaturesModel>? model})
      : super(SeeAllDoctorsRoute.name,
            path: '/see-all-doctors',
            args: SeeAllDoctorsRouteArgs(
                key: key, title: title, showFilter: showFilter, model: model));

  static const String name = 'SeeAllDoctorsRoute';
}

class SeeAllDoctorsRouteArgs {
  const SeeAllDoctorsRouteArgs(
      {this.key,
      required this.title,
      required this.showFilter,
      required this.model});

  final _i30.Key? key;

  final String title;

  final bool showFilter;

  final List<_i31.FeaturesModel>? model;

  @override
  String toString() {
    return 'SeeAllDoctorsRouteArgs{key: $key, title: $title, showFilter: $showFilter, model: $model}';
  }
}

/// generated route for
/// [_i20.MyAccount]
class MyAccountRoute extends _i29.PageRouteInfo<void> {
  const MyAccountRoute() : super(MyAccountRoute.name, path: '/my-account');

  static const String name = 'MyAccountRoute';
}

/// generated route for
/// [_i21.MedicalRecord]
class MedicalRecordRoute extends _i29.PageRouteInfo<void> {
  const MedicalRecordRoute()
      : super(MedicalRecordRoute.name, path: '/medical-record');

  static const String name = 'MedicalRecordRoute';
}

/// generated route for
/// [_i22.Articles]
class ArticlesRoute extends _i29.PageRouteInfo<void> {
  const ArticlesRoute() : super(ArticlesRoute.name, path: '/Articles');

  static const String name = 'ArticlesRoute';
}

/// generated route for
/// [_i23.StackScreen]
class StackScreenRoute extends _i29.PageRouteInfo<void> {
  const StackScreenRoute()
      : super(StackScreenRoute.name, path: '/stack-screen');

  static const String name = 'StackScreenRoute';
}

/// generated route for
/// [_i24.Calender]
class CalenderRoute extends _i29.PageRouteInfo<void> {
  const CalenderRoute() : super(CalenderRoute.name, path: '/Calender');

  static const String name = 'CalenderRoute';
}

/// generated route for
/// [_i25.RequestNurse]
class RequestNurseRoute extends _i29.PageRouteInfo<void> {
  const RequestNurseRoute()
      : super(RequestNurseRoute.name, path: '/request-nurse');

  static const String name = 'RequestNurseRoute';
}

/// generated route for
/// [_i26.BookNurse]
class BookNurseRoute extends _i29.PageRouteInfo<BookNurseRouteArgs> {
  BookNurseRoute({_i30.Key? key, required int id})
      : super(BookNurseRoute.name,
            path: '/book-nurse', args: BookNurseRouteArgs(key: key, id: id));

  static const String name = 'BookNurseRoute';
}

class BookNurseRouteArgs {
  const BookNurseRouteArgs({this.key, required this.id});

  final _i30.Key? key;

  final int id;

  @override
  String toString() {
    return 'BookNurseRouteArgs{key: $key, id: $id}';
  }
}

/// generated route for
/// [_i27.Inbody]
class InbodyRoute extends _i29.PageRouteInfo<void> {
  const InbodyRoute() : super(InbodyRoute.name, path: '/Inbody');

  static const String name = 'InbodyRoute';
}

/// generated route for
/// [_i28.InbodyResult]
class InbodyResultRoute extends _i29.PageRouteInfo<InbodyResultRouteArgs> {
  InbodyResultRoute({_i30.Key? key, required int id})
      : super(InbodyResultRoute.name,
            path: '/inbody-result',
            args: InbodyResultRouteArgs(key: key, id: id));

  static const String name = 'InbodyResultRoute';
}

class InbodyResultRouteArgs {
  const InbodyResultRouteArgs({this.key, required this.id});

  final _i30.Key? key;

  final int id;

  @override
  String toString() {
    return 'InbodyResultRouteArgs{key: $key, id: $id}';
  }
}
