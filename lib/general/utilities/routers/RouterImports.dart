import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/home/HomeImports.dart';
import 'package:base_flutter/general/screens/about/AboutImports.dart';
import 'package:base_flutter/general/screens/active_account/ActiveAccountImports.dart';
import 'package:base_flutter/general/screens/change_password/ChangePasswordImports.dart';
import 'package:base_flutter/general/screens/confirm_password/ConfirmPasswordImports.dart';
import 'package:base_flutter/general/screens/contact_us/ContactUsImports.dart';
import 'package:base_flutter/general/screens/forget_password/ForgetPasswordImports.dart';
import 'package:base_flutter/general/screens/image_zoom/ImageZoom.dart';
import 'package:base_flutter/general/screens/location_address/LocationAddressImports.dart';
import 'package:base_flutter/general/screens/login/LoginImports.dart';
import 'package:base_flutter/general/screens/reset_password/ResetPasswordImports.dart';
import 'package:base_flutter/general/screens/select_lang/SelectLangImports.dart';
import 'package:base_flutter/general/screens/select_user/SelectUserImports.dart';
import 'package:base_flutter/general/screens/splash/SplashImports.dart';
import 'package:base_flutter/general/screens/terms/TermsImports.dart';

import '../../../customer/screens/articles/articles_imports.dart';
import '../../../customer/screens/book_nurse/book_nurse_imports.dart';
import '../../../customer/screens/calender/calender_imports.dart';
import '../../../customer/screens/inbody/inbody_imports.dart';
import '../../../customer/screens/inbody_result/inbody_result_imports.dart';
import '../../../customer/screens/medical_record/medical_record_imports.dart';
import '../../../customer/screens/my_account/my_account_imports.dart';
import '../../../customer/screens/register/register_imports.dart';
import '../../../customer/screens/request_nurse/request_nurse_imports.dart';
import '../../../customer/screens/see_all_doctors/see_all_doctors_imports.dart';
import '../../../customer/screens/see_all_features/see_all_imports.dart';
import '../../../customer/screens/stack/stack_screen_imports.dart';
import '../../../customer/screens/welcome_page/WelcomePageImports.dart';


part 'Router.dart';