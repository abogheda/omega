part of 'TermsImports.dart';

class Terms extends StatefulWidget {
  @override
  _TermsState createState() => _TermsState();
}

class _TermsState extends State<Terms> with TermsData {
  // @override
  // void initState() {
  //   fetchData(context);
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: tr(context, "terms"),
        leading: IconButton(
          icon: Icon(Icons.adaptive.arrow_back),
          onPressed: () {
            AutoRouter.of(context).pop();
          },
        ),
      ),
      body: BuildTermsView(text: ""),
    );
  }
}
