import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/resources/GeneralRepoImports.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:flutter/material.dart';
import 'package:tf_validator/localization/LocalizationMethods.dart';
import 'widgets/TermsWidgetsImports.dart';


part 'TermsData.dart';

part 'Terms.dart';
