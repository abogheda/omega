part of 'TermsWidgetsImports.dart';

class BuildTermsView extends StatelessWidget {
  final String text;

  const BuildTermsView({required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 15),
      child: ListView(
        // mainAxisSize: MainAxisSize.min,
        children: [
          HeaderLogo(),
          MyText(title: "If you’re offered a seat on a rocket ship, don’t ask what seat! Just get on board and move the sail towards the destination. If you’re offered a seat on a rocket ship, don’t ask what seat! Just get on board and move the sail towards the destination.",
            color: MyColors.black, size: 12,),
        ],
      ),
    );
  }
}
