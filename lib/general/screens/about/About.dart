part of 'AboutImports.dart';


class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> with AboutData {


  // @override
  // void initState() {
  //   fetchData(context);
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: tr(context, "about"),
        leading: IconButton(
        icon: Icon(Icons.adaptive.arrow_back),
        onPressed: (){
          AutoRouter.of(context).pop();
        },
      ),
      ),
      body:  BuildAboutView(text: ''),

    );
  }
}
