import 'package:auto_route/auto_route.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:base_flutter/general/resources/GeneralRepoImports.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:tf_validator/tf_validator.dart';
import 'widgets/AboutWidgetsImports.dart';
import 'package:flutter/material.dart';

part 'About.dart';

part 'AboutData.dart';
